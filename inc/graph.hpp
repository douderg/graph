#ifndef _GRAPH_HPP_
#define _GRAPH_HPP_

#include <memory>
#include <unordered_map>
#include <forward_list>

namespace tbd {
namespace helpers {

template <class T> class weak_owner;

template <class T>
class handler {
	friend class weak_owner<T>;
public:

	handler() {}

	handler(const T& val): data_{new T{val}} {}

	handler(const std::shared_ptr<T>& ptr): data_{ptr} {}

	handler(std::shared_ptr<T>&& ptr): data_{std::move(ptr)} {}

	bool operator==(const handler& other) const {
		return (data_ && other.data_) ? *data_ == *other.data_ : (!data_ && !other.data_);
	}

	bool operator!=(const handler& other) const {
		return ! (*this == other);
	}

	struct hash {
		size_t operator()(const handler& h) const {
			return std::hash<T>{}(h.data_? *h.data_ : T{});
		}
	};

private:
	std::shared_ptr<T> data_;
};

template <class T>
class weak_owner {
public:
	weak_owner(): data_{} {}
	weak_owner(const handler<T>& h): data_{h.data_} {}
	weak_owner(const std::shared_ptr<T>& data): data_{data} {}

	handler<T> lock() const {
		return data_.lock();
	}
private:
	std::weak_ptr<T> data_;
};

template <class V, class E>
struct graph_helper {
	typedef typename std::conditional<std::is_void<E>::value, weak_owner<V>, std::pair<weak_owner<V>, handler<E>>>::type edge_t;
	typedef std::unordered_map<handler<V>, std::forward_list<edge_t>, typename handler<V>::hash> storage_t;
};

template <class V, class E>
class find_vertex {
public:
	find_vertex(const handler<V>& ref):ref_{ref} {}
	bool operator()(const typename graph_helper<V, E>::edge_t& edge) const {
		return edge.first.lock() == ref_;
	}
private:
	const handler<V>& ref_;
};

template <class V>
class find_vertex<V, void> {
public:
	find_vertex(const handler<V>& ref):ref_{ref} {}
	bool operator()(const typename graph_helper<V, void>::edge_t& edge) const {
		return edge.lock() == ref_;
	}
private:
	const handler<V>& ref_;
};

} // helpers


template <class V, class E = void>
class graph {
	typedef typename helpers::graph_helper<V, E>::storage_t storage_t;
public:
	graph(): nodes_{new storage_t{}} {}

	graph& add_vertex(const V& vertex) {
		(*nodes_)[vertex];
		return *this;
	}

	template <class T = E>
	typename std::enable_if<std::is_convertible<T, E>::value && !std::is_void<E>::value, graph&>::type 
	add_edge(const V& from, const V& to, const T& edge) {
		auto dst = nodes_->find(to);
		if (dst == nodes_->end()) {
			throw std::runtime_error("invalid destination node");
		}
		
		auto src = nodes_->find(from);
		if (src == nodes_->end()) {
			throw std::runtime_error("invalid source node");
		}

		typename helpers::find_vertex<V, E> eq(dst->first);
		if (std::find_if(src->second.begin(), src->second.end(), eq) == src->second.end()) {
			src->second.push_front(std::make_pair(dst->first, edge));
		}

		return *this;
	}

	template <class T = void>
	typename std::enable_if<std::is_same<T, E>::value && std::is_void<E>::value, graph&>::type 
	add_edge(const V& from, const V& to) {
		auto dst = nodes_->find(to);
		if (dst == nodes_->end()) {
			throw std::runtime_error("invalid destination node");
		}
		auto src = nodes_->find(from);
		if (src == nodes_->end()) {
			throw std::runtime_error("invalid source node");
		}

		typename helpers::find_vertex<V, void> compare(dst->first);
		if (std::find_if(src->second.begin(), src->second.end(), compare) == src->second.end()) {
			src->second.push_front(dst->first);
		}
		return *this;
	}

	graph& remove_vertex(const V& vertex) {
		nodes_->erase(vertex);
		return *this;
	}

	graph& remove_edge(const V& from, const V& to) {
		auto src = nodes_->find(from);
		if (src == nodes_->end()) {
			return *this;
		}

		auto dst = nodes_->find(to);
		if (dst == nodes_->end()) {
			return *this;
		}

		typename helpers::find_vertex<V, E> eq(dst->first);
		auto it1 = src->second.before_begin(), it2 = src->second.begin();
		while (it2 != src->second.end()) {
			if (eq(*it2)) {
				src->second.erase_after(it1);
				return *this;
			}
			it1 = it2++;
		}
		return *this;
	}

private:
	std::shared_ptr<storage_t> nodes_;
};

}

#endif